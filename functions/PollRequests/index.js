'use strict';

// AWS Library
var AWS = require('aws-sdk'),

// UUID Library
uuid = require('uuid'),

// DynamoDB Client
documentClient = new AWS.DynamoDB.DocumentClient();

// Handle Incoming Request
exports.handler = function(event, context, callback) {

    // TODO: Verify API Key in the Query String is a valid key in a DynamoDB table

    // TODO: Validate the API Gateway Request ensuring that required parameters are set w/ appropriate data types/formats, etc.

    // Build params object for DynamoDB
    var params = {
        TableName: process.env.TABLE_NAME,
        Item: {
            "id": uuid.v1(), // Unique UUID for the Request PK
            "created_at": Math.floor(new Date() / 1000), // Current UNIX Timestamp
            "payload": JSON.stringify(event["body-json"]), // JSON Request Body
            "params": JSON.stringify(event.params), // Path, Query String, Headers
            "context": JSON.stringify(event.context), // Request Context (User Agent, IP Address, etc.)
            "stage_variables": JSON.stringify(event.stage_variables), // API Gateway Stage Variables (i.e. API Gateway Environmental Variables)
        }
    };

    // Put params object in DynamoDB record
    documentClient.put(params, function(err, data){
        callback(err, data);
    });

}

/*
 * Lambda Notes:
 *
 * - Access Lambda ENV variables: process.env.TABLE_NAME (These are set in the UI)
 * - The Lambda context variable contains good debug info, this should probably be logged somewhere
 *
 *
 */
 